package com.mrcrayfish.tutorial.items;

import com.mrcrayfish.tutorial.Reference;

import net.minecraft.item.Item;

public class ItemSeedbomb extends Item {
	
		public ItemSeedbomb(){
			setUnlocalizedName(Reference.TutorialItems.SEEDBOMB.getUnlocalizedName());
			setRegistryName(Reference.TutorialItems.SEEDBOMB.getRegistryName());
		}
}
