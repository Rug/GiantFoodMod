package com.mrcrayfish.tutorial.items;

import com.mrcrayfish.tutorial.Reference;

import net.minecraft.item.Item;

public class ItemCracker extends Item {
	
	public ItemCracker() {
		setUnlocalizedName(Reference.TutorialItems.CRACKER.getUnlocalizedName());
		setRegistryName(Reference.TutorialItems.CRACKER.getRegistryName());
	}
}
